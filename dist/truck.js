var utils = require('utils');

module.exports = function(truck) {
    if(truck) {
        if(!truck.memory.mode || truck.memory.mode == 'pickup'){
            var droppedEnergy = utils.findQ(truck.room, FIND_DROPPED_ENERGY);
            if(droppedEnergy.length) {
                truck.moveTo(droppedEnergy[0]);
                truck.pickup(droppedEnergy[0]);
                if(truck.carry.energy == truck.carryCapacity) {
                    truck.memory.mode = 'drop';
                }
            }
        } else if(truck.memory.mode == 'drop'){
            var target = getDropSite(truck.room);
            truck.moveTo(target);
            truck.transferEnergy(target);
            if(truck.carry.energy == 0) {
                truck.memory.mode = 'pickup';
            }
        }
    } else {
        console.warn('Trying to use non-existant creep');
    }
}

function getDropSite(room) {
    //SPAWN + EXT not full, fill it
    if(room.energyAvailable < room.energyCapacityAvailable) {
        var spawns = utils.findQ(room, FIND_MY_SPAWNS);
        for(var s in spawns) {
            var spawn = spawns[s];
            if(spawn.energy < spawn.energyCapacity) {
                return spawn;
            }
        }
        var extensions = room.find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_EXTENSION }
        });
        for(var e in extensions) {
            var ext = extensions[e];
            if(ext.energy < ext.energyCapacity) {
                return ext;
            }
        }
    // SPAWN FULL, fill Storage    
    } else if (room.storage) {
         if(room.storage.store.energy < room.storage.storeCapacity) {
            return room.storage;
         }
    }
    //return room.controller;
}