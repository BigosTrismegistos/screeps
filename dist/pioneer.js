var utils = require('utils');

module.exports = function(creep) {
  var room = creep.room;
  //room unclaimed
  if(room.controller && 
     room.controller.owner.username !== 'Bigos') {
    creep.moveTo(room.controller);
    creep.claimController(room.controller);
  // room claimed, contruction present
  } else {
    if(creep.memory.mode != 'working') {
      var sources = utils.findQ(room, FIND_SOURCES);
      if(sources.length > 0) {
        creep.moveTo(sources[0]);
        creep.harvest(sources[0]);
        if(creep.carry.energy === creep.carryCapacity) {
          creep.memory.mode = 'working';
        }
      }
    } else {
      var sites = utils.findQ(room, FIND_CONSTRUCTION_SITES);
      if(sites.length > 0) {
        creep.moveTo(sites[0]);
        creep.build(sites[0]);
      } else {
        creep.moveTo(room.controller);
        creep.upgradeController(room.controller);
      }
      if(creep.carry.energy === 0) {
        creep.memory.mode = 'harvesting';
      }
    }
  }
};