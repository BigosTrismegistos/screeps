var _ = require('lodash');
var manual = require('manual');
var utils = require('utils');
var roomLevel = require('roomLevel');
var prototypes = require('prototypes');

module.exports = {
    rebuildCreeps: function(room) {
        var crps = utils.findQ(room, FIND_MY_CREEPS);
        var existingRoles = crps.reduce(function(acc, creep, idx){
            acc[creep.memory.role] ? acc[creep.memory.role]++ : acc[creep.memory.role] = 1;
            return acc;
        }, []);
        //var s = '';
        //for(var role in existingRoles) s += role + ': ' + existingRoles[role] + ', ';
        //console.log(s)
        var config = roomLevel.getRoomConfig(room);
        if(!existingRoles['miner'] || existingRoles['miner'] < config.units['miner']) {
            //console.log('too few miners, prioritizing');
            increaseNumber('miner', room, config);
        } else {
            _.forEach(config.units, function(targetNumber, role){
                if(!existingRoles[role]) { existingRoles[role] = 0; }
                if(existingRoles[role] < targetNumber) {
                    increaseNumber(role, room, config);
                }
            });   
        }
    },
    clearMemory: function(){
        //console.log(Game.time);
        if(Game.time % 1000 === 0) {
            for(var i in Memory.creeps) {
                if(!Game.creeps[i]) {
                    delete Memory.creeps[i];
                }
            }
        }
    },
    clearCaches: function() {
        utils.clearRoomCache();
    },
    extendPrototypes: function() {
        prototypes();
    }
};

function increaseNumber(role, room, roomConfig) {
  var body = roomConfig.unitBodies[role];
  var spawns = utils.findQ(room, FIND_MY_SPAWNS);
  for(var s in spawns){
      var spawn = spawns[s];
      if(spawn.canCreateCreep(body) === 0) {
          spawn.createCreep(body, null, { role : role });
        break;
      } else {
          //console.log('Cant create creep: ' + role);
      }
  }
}
