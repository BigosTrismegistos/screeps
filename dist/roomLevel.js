var overrides = require('roomOverrides');

module.exports = {
    getRoomLevel: function(room) {
        var roomEnergy = room.energyAvailable;
        if(roomEnergy <= 200) {
            return 0; // RECOVERY MODE
        } else if(roomEnergy > 200 && roomEnergy < 550) {
            return 1;
        } else if(roomEnergy >= 550 && roomEnergy < 800) {
            return 2;
        } else if(roomEnergy >= 800 && roomEnergy < 1300) {
            return 3;
        } else if(roomEnergy >= 1300 && roomEnergy < 1800) {
            return 4;
        } else if(roomEnergy >= 1800 && roomEnergy < 2300) {
            return 5;
        } else if(roomEnergy >= 2300 && roomEnergy < 2800) {
            return 6;
        } else if(roomEnergy >= 2800 && roomEnergy < 3500) {
            return 7;
        } else if(roomEnergy >= 3500) {
            return 8;
        } else return 0;
    },
    getRoomConfig: function(room) {
        var roomConfig = this.config[this.getRoomLevel(room)];
        return this.applyOverrides(roomConfig, room);
    },
    config: {
        0: {
            units: {
                miner: 1,
                truck: 0,
                builder: 0,
                maint: 0,
                guard: 0
            },
            unitBodies: {
                miner: [WORK, CARRY, MOVE]
            }
        },
        1: {
            units: {
                miner: 1,
                truck: 1,
                builder: 1,
                maint: 0,
                guard: 0
            },
            unitBodies: {
                truck: [CARRY, CARRY, MOVE],  
                miner: [WORK, WORK, CARRY, MOVE],
                builder: [WORK, CARRY, CARRY, MOVE, MOVE]
            }
        },
        2: {
            units: {
                miner: 1,
                truck: 1,
                builder: 2,
                maint: 1,
                guard: 0
            },
            unitBodies: {
                miner: [WORK, WORK, WORK, WORK, CARRY, MOVE],
                truck: [CARRY, CARRY, CARRY, MOVE, MOVE, MOVE],
                builder: [WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE],
                maint: [WORK, CARRY, MOVE]
            }
        },
        3: {
            units: {
                miner: 1,
                truck: 2,
                builder: 2,
                maint: 1,
                guard: 0
            },
            unitBodies: {
                miner: [WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE],
                truck: [CARRY, CARRY, CARRY, MOVE, MOVE, MOVE],
                builder: [WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE],
                maint: [WORK, CARRY, MOVE]
            }
        },
        4: {
            units:{
                miner: 1,
                truck: 2,
                builder: 2,
                maint: 1,
                guard: 1
            },
            unitBodies: {
                truck: [CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],
                miner: [WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE],
                builder: [WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE],
                maint: [WORK, WORK, CARRY, MOVE, MOVE],
                guard: [ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE]
            }
        },
        5: {
            units:{
                miner: 1,
                truck: 2,
                builder: 2,
                maint: 1,
                guard: 1
            },
            unitBodies: {
                truck: [CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],
                miner:  [WORK, WORK, WORK, WORK, WORK, WORK, CARRY, MOVE],
                builder: [WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE],
                maint: [WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE],
                guard: [ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE]
            }
        }
    },
    applyOverrides: function(config, room) {
        var ovrrds = overrides[room.name][this.getRoomLevel(room)]; 
        for(var prop in ovrrds) {
            if(typeof config[prop] === 'object') {
                for(var innerProp in ovrrds[prop]) {
                    //console.log('Override ' + prop + '.' + innerProp + ', ' + config[prop][innerProp] + ' for ' + ovrrds[prop][innerProp]);
                    config[prop][innerProp] = ovrrds[prop][innerProp];
                }
            } else {
                //console.log('Override ' + prop + ', ' + config[prop] + ' for ' + ovrrds[prop]);
                config[prop] = ovrrds[prop];
            }
        }  
        return config;
    }
};