var utils = require('utils');
var roomLevel = require('roomLevel');

module.exports = function(builder) {
    //BUILD MODE
    if(builder.carry.energy > 0) {
        var sites = utils.findQ(builder.room, FIND_CONSTRUCTION_SITES);
        if(sites.length) {
            var site = chooseSite(sites);
            builder.moveTo(site);
            builder.build(site);
        } else {
            builder.moveTo(builder.room.controller);
            builder.upgradeController(builder.room.controller);
        }
    // COLLECT MODE    
    } else {
        var takeFromSpawns = builder.room.storage == null;
        var storage = utils.getFullStorage(builder.room, takeFromSpawns);
        if(storage) {
            builder.moveTo(storage);
            storage.transferEnergy(builder, Math.min(storage.energy, builder.carryCapacity));
        }
    }
};

function chooseSite(sites) {
    return sites[0];
}