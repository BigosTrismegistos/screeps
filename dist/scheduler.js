var _ = require('lodash');

module.exports = {
    
    delay: function(eventName, params, delay) {
        //console.log('Delaying ' + eventName + ' at ' + Game.time + ' for ' + delay);
        this.scheduleAt(eventName, params, Game.time + delay);
    },
    scheduleAt: function(eventName, params, gameTime) {
        var eventsMap = getEventsMap();
        if (!eventsMap[gameTime]) {
            eventsMap[gameTime] = [];  
        }
        eventsMap[gameTime].push({ name: eventName, params: params});
        Memory.eventsMap = eventsMap;
    },
    
    runScheduled: function() {
        var eventsMap = getEventsMap();
        var ticksToRun = _.pick(eventsMap, function(evt, time) {
            return time <= Game.time;
        });
        
        while(Object.keys(ticksToRun).length > 0) {
            var tickKey = Object.keys(ticksToRun)[0];
            var tickEvents = ticksToRun[tickKey];
            while(tickEvents.length > 0) {
                var evt = tickEvents.shift();
                this.runEvent(evt);
            }
            delete eventsMap[tickKey];
            delete ticksToRun[tickKey];
        }
        
        //Memory.
    },
    
    runEvent: function(event) {
        //console.log('Event run: ' + event.name);
        //console.log('event: ' + JSON.stringify(event))
        switch(event.name) {
            case 'EMPTY_SOURCE':
                //console.log('emptying source');
                var source = Game.getObjectById(event.params);
                source.room.memory.sourcesMap[source.id] = null;
                break;
        }
    }
};

function getEventsMap() {
    var map = Memory.eventsMap;
    if(typeof map !== 'object') {
        Memory.eventsMap = {};
        map = {};
    }
    return map;
};
