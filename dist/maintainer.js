var utils = require('utils');
var _ = require('lodash');
var manual = require('manual');

module.exports = function(maint){
    if (maint.carry.energy == 0) {
        var storage = utils.getFullStorage(maint.room, true);
        if(storage) {
            maint.moveTo(storage);
            storage.transferEnergy(maint, Math.min(maint.carryCapacity, storage.energy));
        }
    } else if(maint.memory.mode == 'repairing') {
        var target = Game.getObjectById(maint.memory.targetId);
        maint.moveTo(target);
        maint.repair(target);
        // REPAIR FINISHED
        if(target == null || target.hits >= manual.getRepairTargetHP(target.structureType)){
            maint.memory.mode = '';
        }
    } else {
        var target = utils.getNextRepairTarget(maint.room);
        if(target){
            maint.memory.targetId = target.id;
            maint.memory.mode = 'repairing';
        }
    }
};