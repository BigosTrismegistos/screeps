var miners = require('miners');
var truck = require('truck');
var builder = require('builder');
var manual = require('manual');
var maintenance = require('maintenance');
var maint = require('maintainer');
var military = require('military');
var _ = require('lodash');
var pioneer = require('pioneer');
var scheduler = require('scheduler');

maintenance.clearCaches();
maintenance.clearMemory();
maintenance.extendPrototypes();

for(var s in Game.spawns) {
    var spawn = Game.spawns[s];
    maintenance.rebuildCreeps(spawn.room);
}

for(var name in Game.creeps) {
    var creep = Game.creeps[name];
    creep.room.setupRoom();
    if(!creep.spawning) {
        if(creep.memory.role === 'miner') {
            miners(creep);
        } else if(creep.memory.role === 'truck') {
            truck(creep);
        } else if(creep.memory.role === 'builder') {
            builder(creep);
        } else if (creep.memory.role === 'maint') {
            maint(creep);
        } else if(creep.memory.role === 'guard') {
            military.guard(creep);
        } else if(creep.memory.role === 'pioneer') {
          pioneer(creep);
        }
    }
}

scheduler.runScheduled();