var _ = require('lodash');
var utils = require('utils');
var roomLevel = require('roomLevel');
var scheduler = require('scheduler');

module.exports = function (miner) {
    if (!miner.memory.sourceId) { //OBTAIN SOURCEID IF NOT PRESENT
        var sources = miner.room.getUnassignedSources();
        if (Object.keys(sources).length > 0) {
            var freeSourceId = Object.keys(sources)[0];
            miner.memory.sourceId = freeSourceId;
            miner.room.memory.sourcesMap[freeSourceId] = miner.id;
            scheduler.delay('EMPTY_SOURCE', freeSourceId, miner.ticksToLive);
        }
    }
    if(miner.memory.sourceId) { // HARVEST PART
        var source = Game.getObjectById(miner.memory.sourceId)
        if(!source) { //SOURCE NOT FOUND, DEFAULT TO NO SOURCE
            miner.memory.sourceId = null;
        } else { // SOURCE FOUND, HARVEST
          if(!miner.memory.mode || miner.memory.mode === 'harvesting') {
                miner.moveTo(source);
                miner.harvest(source);
                if(miner.carry.energy == miner.carryCapacity) {
                  miner.memory.mode = 'full';
                }
            } else {
                if(roomLevel.getRoomLevel(miner.room) === 0) {
                    //PART FOR EMERGENCY MODE / 0 LEVEL ROOM
                    var spawns = utils.findQ(miner.room, FIND_MY_SPAWNS);
                    if(spawns.length > 0 && _.some(spawns, function(s){ return s.energyCapacity > s.energy; })) {
                        miner.moveTo(spawns[0]);
                        miner.transferEnergy(spawns[0]);
                    } else {
                        miner.moveTo(miner.room.controller);
                        miner.upgradeController(miner.room.controller);
                    }
                } else { //PART FOR MINER-RUNNER CHAINING
                    miner.dropEnergy(miner.carry.energy);
                }
                if(miner.carry.energy === 0) {
                    miner.memory.mode = 'harvesting';
                }  
            }
        }
    }
};