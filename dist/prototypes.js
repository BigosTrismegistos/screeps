var manual = require('manual');
var _ = require('lodash');

module.exports = function () {
    
    Room.prototype.setupRoom = function () {
        if (!this.memory.sources) {
            var sources = this.find(FIND_SOURCES);
            this.memory.sources = sources;
        }
        if (!this.memory.sourcesMap) {
            this.memory.sourcesMap = {};
            for (var s in this.memory.sources) {
                var source = this.memory.sources[s];
                this.memory.sourcesMap[source.id] = null;
            }
        }
    };
    
    Room.prototype.getUnassignedSources = function() {
        var sourcesMap = this.memory.sourcesMap;
        var unassigned = _.omit(sourcesMap, function(creepId, sourceId) {
            return creepId !== null;
        });
        return unassigned;
    };
    
    Creep.prototype.pathfind = function (targetPos) {
        var pathArray = this.pos.findPathTo(targetPos);
        return pathArray;
    };

    Creep.prototype.continueByPath = function () {
        //console.log('Path length: ' + this.memory.path.length + ', first direction: ' + this.memory.path[0].direction);
        if(this.memory.path && this.memory.path.length > 0) {
            var moveResult = this.moveByPath(this.memory.path);
            if (moveResult !== OK) {
                console.log('Creep ' + this.name +
                            ', the ' + this.memory.role +
                            ' cannot move: ' + moveResult);
                if(moveResult === ERR_NOT_FOUND ||
                   moveResult === ERR_INVALID_ARGS) {
                    //console.log('REROUTING BECAUSE OF ERROR');
                    this.reroute(this.memory.pathTarget);
                }
            }
        }
    };
    
    Creep.prototype.reroute = function (newTargetPos) {
        //console.log('rerouting to: ' + JSON.stringify(newTarget));
        var gamePos = Game.rooms[newTargetPos.roomName].getPositionAt(
            newTargetPos.x, newTargetPos.y);
        this.memory.pathTarget = gamePos;
        this.memory.path = this.pathfind(gamePos);
        this.continueByPath();
    };

    Creep.prototype.goTo = function (target) {
        var pathTarget = this.memory.pathTarget;
        //console.log('goTo called, target: ' +
        //            JSON.stringify(target.pos) +
        //            ', pathTarget: ' + JSON.stringify(pathTarget));
        if (pathTarget) { //THERE IS A TARGET
            if (target.pos.isEqualTo(pathTarget.x, pathTarget.y)) { //IT'S THE SAME TARGET
                //console.log('continuing path');
                if (this.pos.isEqualTo(target.pos)) { // ALREADY THERE
                    this.memory.pathTarget = null;
                    this.memory.path = null;
                    return;
                } else { // GO
                    this.continueByPath();
                }
            } else { // IT'S A NEW TARGET
                this.reroute(target.pos);
            }
        } else { // START GOING THERE
            this.reroute(target.pos);
        }
    };

    //TODO: move repair target HP to roomConfig and remove manual use here
    Structure.prototype.needsRepair = function () {
        var targetHits = Math.min(
            manual.getRepairTargetHP(this.structureType),
            this.maxHits
        );
        return this.hits < targetHits;
    };
};