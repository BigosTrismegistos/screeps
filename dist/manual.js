var valueObject = {
  truckBody_: [CARRY, CARRY, MOVE, MOVE, MOVE],
  minerBody_: [WORK, WORK, CARRY, MOVE],
  builderBody_: [WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE],
  maintBody_: [WORK, WORK, CARRY, MOVE, MOVE],
  guardBody_: [ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE],
  
  roomConfigs: {
    'E15N4': {
        strongpoints: [ {x: 2, y: 28} ]
    }  
  },
  
  repairTargetHp_: {
  },
  
  repairTriggerHp_: {
  },
  
  getRepairTargetHP: function(type) {
      return this.repairTargetHp_[type] ? this.repairTargetHp_[type] : Infinity;
  },
  
  getRepairTriggerHP: function(type) {
      return this.repairTriggerHp_[type] ? this.repairTriggerHp_[type] : Infinity;
  },
  
  buildTruck: function(spawn) {
      var truck = spawn.createCreep(this.truckBody_, null, { role: 'truck'});
      console.log(truck > 0 ? 'Truck produced' : truck);
  },  
  buildMiner: function(spawn){
      var miner = spawn.createCreep(this.minerBody_, null, { role: 'miner'});
      console.log(miner > 0 ? 'Miner produced' : miner);
  },
  buildBuilder: function(spawn) {
      var builder = spawn.createCreep(this.builderBody_, null, { role: 'builder'});
      console.log(builder > 0 ? 'Builder produced' : builder);
  },
  buildMaint: function(spawn) {
      var maint = spawn.createCreep(this.maintBody_, null, { role: 'maint'});
      console.log(maint > 0 ? 'Maint produced' : maint);
  },
  buildGuard: function(spawn) {
      var guard = spawn.createCreep(this.guardBody_, null, { role: 'guard'});
      console.log(guard > 0 ? 'Guard produced' : guard);
  }
}


valueObject.repairTargetHp_[STRUCTURE_ROAD] = 5000;
valueObject.repairTargetHp_[STRUCTURE_WALL] = 20000;
valueObject.repairTargetHp_[STRUCTURE_RAMPART] = 60000;
valueObject.repairTargetHp_[STRUCTURE_EXTENSION] = 1000;
valueObject.repairTargetHp_[STRUCTURE_CONTROLLER] = 0;
valueObject.repairTargetHp_[STRUCTURE_STORAGE] = 10000;

valueObject.repairTriggerHp_[STRUCTURE_ROAD] = 2000;
valueObject.repairTriggerHp_[STRUCTURE_WALL] = 19999;
valueObject.repairTriggerHp_[STRUCTURE_RAMPART] = 55000;

module.exports = valueObject;