var overridesMap = {
    'W6S2': {
        2: {
            units: {
                miner: 2
            }
        },
        3: {
            units: {
                miner: 2
            },
            unitBodies: {
                builder: [WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE]
            }
        },
        4: {
            units: {
                miner: 2
            }
        },
        5: {
            units: {
                miner: 2
            }
        },
        6: {
            units: {
                miner: 2
            }
        }
    },
    'sim': {
    }
}

module.exports = overridesMap;