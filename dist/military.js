var manual = require('manual');
var utils = require('utils');

module.exports = {
    guard: function (guard) {
        var hostiles = utils.findQ(guard.room, FIND_HOSTILE_CREEPS);
        if (hostiles.length) {
            notifyByEmail(hostiles);
            hostiles = hostiles.sort(function(e1, e2){
               return e1.hits - e2.hits; 
            });
            var target = hostiles[0];
            if (target) {
                guard.moveTo(target);
                guard.attack(target);
            }
        } else {
            // TODO: choosing points
            //var deployPoint = manual.roomConfigs[guard.room.name].strongpoints[0];
            //guard.moveTo(deployPoint.x, deployPoint.y);
        }
    }
}

function notifyByEmail(hostiles){
    var hostilesCount = {};

    hostiles.filter(function(i) { 
        if(i.owner.username != 'Source Keeper') {
            hostilesCount[i.owner.username] = hostilesCount[i.owner.username] || 0;
            hostilesCount[i.owner.username]++;
        }
    });
    
    for(var user in hostilesCount) {
        Game.notify(hostilesCount[user] + ' enemies spotted: user ' + user);
    }
}