var _ = require('lodash');
var manual = require('manual');

var roomCache = {};

module.exports = {
    getFullSpawnOrExtension: function(room) {
        var extensions = room.find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_EXTENSION }
        });
        var extension = _.find(extensions, function(ext){ return ext.energy > 0 });
        if(extension) return extension;
        var spawns = this.findQ(room, FIND_MY_SPAWNS);
        for(var s in spawns) {
            var spawn = spawns[s];
            if(spawn.energy > 0) {
                return spawn;
            }
        }
    },
    getFullStorage: function(room, opt_includeSpawn) {
        if(room.storage && room.storage.store.energy > 0) {
            return room.storage;
        } else if(opt_includeSpawn) {
            return this.getFullSpawnOrExtension(room);
        } else return null;
    },
    findQ: function(room, structureType, opts) {
        if(roomCache[room.name] && roomCache[room.name][structureType]){
            return roomCache[room.name][structureType];
        } else {
            var found = room.find(structureType);
            if(!roomCache[room.name]){
                roomCache[room.name] = {};
            }
            roomCache[room.name][structureType] = found;
            return found;
        }
    },
    getNextRepairTarget: function(room){
        if(!roomCache[room.name] || roomCache[room.name] && !roomCache[room.name].repairQueue){
            if(!roomCache[room.name]) roomCache[room.name] = {};
            var structures = this.findQ(room, FIND_STRUCTURES);
            structures = structures.filter(function(s) {
                var repairable = s.structureType != STRUCTURE_CONTROLLER &&
                    s.structureType != STRUCTURE_KEEPER_LAIR &&
                    s.structureType != STRUCTURE_SPAWN;
                
                if (repairable && s.structureType == STRUCTURE_WALL && s.ticksToLive > 0) {
                    repairable = false;
                }
                return repairable;
            });
            structures = structures.sort(function(s1, s2){
                var s1targetHP = manual.getRepairTargetHP(s1.structureType);
                s1targetHP = s1targetHP ? s1targetHP : s1.maxHits;
                var s2targetHP = manual.getRepairTargetHP(s2.structureType);
                s2targetHP = s2targetHP ? s2targetHP : s2.maxHits;
                var s1P = s1.hits / s1targetHP;
                var s2P = s2.hits / s2targetHP;

                return s1P - s2P;

            });
            roomCache[room.name].repairQueue = structures;
        }
        return roomCache[room.name].repairQueue.shift();
    },
    clearRoomCache: function() {
        for(var i in roomCache) { 
            console.log(i + ', ' + roomCache[i]);
            delete roomCache[i];
        }
    }
};

